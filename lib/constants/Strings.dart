class Strings {
  static const applicationName = "Physique";
  static const schedule = "Schedule";
  static const settings = "Settings";
  static const workouts = _WorkoutPaths();
}

class _WorkoutPaths {
  String get bentLegRaise => "assets/workouts/bent-leg-raise.jpg";
  String get bentOverTwist => "assets/workouts/bent-over-twist.jpg";
  String get calfRaise => "assets/workouts/calf-raise.jpg";
  String get chinUp => "assets/workouts/chin-up.jpg";
  String get chairDip => "assets/workouts/dip-between-chairs.jpg";
  String get pushUp => "assets/workouts/push-up.jpg";
  String get chairRow => "assets/workouts/row-between-chairs.jpg";
  String get sitUp => "assets/workouts/sit-up.jpg";
  String get squats => "assets/workouts/squats.jpg";

  const _WorkoutPaths();
}
