import 'package:flutter/material.dart'
    show BuildContext, Container, ListView, PageController, PageView, State, StatefulWidget, Widget;
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart' show CalendarCarousel;
import 'package:physique/models/models.dart' show WorkoutCollection;

class PhysiqueSchedule extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PhysiqueScheduleState();
}

class _PhysiqueScheduleState extends State<PhysiqueSchedule> {
  static CalendarCarousel _monthCalendar;
  static CalendarCarousel _weekCalendar;
  PageView _pageView;
  List<CalendarCarousel> _scheduleViews;
  DateTime selectedDate;

  _PhysiqueScheduleState({this.selectedDate});

  @override
  Widget build(BuildContext context) {
    _monthCalendar = CalendarCarousel<WorkoutCollection>(
      onDayPressed: (DateTime date, List<WorkoutCollection> workouts) {
        setState(() {
          selectedDate = date;
        });
        _pageView.controller.jumpToPage(_scheduleViews.indexOf(_weekCalendar));
      },
      weekFormat: false,
      selectedDateTime: selectedDate,
    );
    _weekCalendar = CalendarCarousel<WorkoutCollection>(
      onDayPressed: (DateTime date, List<WorkoutCollection> workouts) {
        if (date == selectedDate) {
          setState(() {
            selectedDate = null;
          });
          _pageView.controller.jumpToPage(_scheduleViews.indexOf(_monthCalendar));
        } else {
          // TODO display workout completion for $date
        }
      },
      weekFormat: true,
      selectedDateTime: selectedDate,
    );
    _scheduleViews = <CalendarCarousel>[_monthCalendar, _weekCalendar];

    _pageView = PageView.builder(
        itemBuilder: (context, position) => _scheduleViews[position],
        itemCount: _scheduleViews.length,
        controller: PageController(
            initialPage: selectedDate == null
                ? _scheduleViews.indexOf(_monthCalendar)
                : _scheduleViews.indexOf(_weekCalendar),
            keepPage: true),
        onPageChanged: (position) {});

    return ListView(children: <Widget>[
//      ListTile(
//          title: Text(Strings.scheduleView),
//          trailing: DropdownButton<String>(
//              value: _selectedScheduleView,
//              onChanged: (String newValue) {
//                if (_selectedScheduleView != newValue) {
//                  setState(() {
//                    _selectedScheduleView = newValue;
//                    _pageView.controller.jumpToPage(_scheduleViews.indexOf(newValue));
//                  });
//                }
//              },
//              items: _scheduleViews
//                  .map<DropdownMenuItem<String>>((String mode) =>
//                      DropdownMenuItem<String>(value: mode, child: Text("${mode}ly")))
//                  .toList(growable: false))),
//      Divider(),
      Container(height: 400, child: _pageView)
    ]);
  }
}
