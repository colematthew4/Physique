import 'package:flutter/material.dart' show Key, StatefulBuilder, Text, Widget;
import 'package:physique/constants/constants.dart' show PhysiqueDashboardTab;

import 'PhysiqueSchedule.dart';

typedef WidgetSupplier = Widget Function();

final _widgetCache = <PhysiqueDashboardTab, WidgetSupplier>{
  PhysiqueDashboardTab.schedule: () => PhysiqueSchedule(),
  PhysiqueDashboardTab.settings: () => Text("Settings")
};
final _pageBuilderCache = <PhysiqueDashboardTab, StatefulBuilder>{};

class DashboardPageBuilder extends StatefulBuilder {
  DashboardPageBuilder._create(PhysiqueDashboardTab tab, [Key key])
      : super(key: key, builder: (context, setState) => _widgetCache[tab]());

  factory DashboardPageBuilder(int index) {
    PhysiqueDashboardTab tab = PhysiqueDashboardTab.values[index];
    return _pageBuilderCache[tab] ??= DashboardPageBuilder._create(tab);
  }
}
