import 'package:flutter/material.dart' show Icon, Icons, Widget, hashValues;
import 'package:flutter_calendar_carousel/classes/event.dart' show EventInterface;
import 'package:physique/models/models.dart' show CompletableWorkout;

class WorkoutCollection extends EventInterface {
  final workouts = <CompletableWorkout>{};
  final String _title;
  final Widget _dot;
  static const Widget _icon = Icon(Icons.fitness_center);

  WorkoutCollection(this._title, this._dot);

  bool isComplete() => workouts.every((element) => element.isComplete());

  @override
  DateTime getDate() => null;

  @override
  Widget getDot() => _dot;

  @override
  Widget getIcon() => _icon;

  @override
  String getTitle() => _title;

  @override
  bool operator ==(dynamic other) {
    return other is EventInterface &&
        this.getDate() == other.getDate() &&
        this._title == other.getTitle() &&
        _icon == other.getIcon() &&
        this._dot == other.getDot();
  }

  @override
  int get hashCode => hashValues(_title, _icon);
}
