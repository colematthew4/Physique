import 'package:flutter/material.dart' show Image, hashValues;
import 'package:physique/constants/constants.dart' show Strings;

final workouts = <CompletableWorkout>{
  WorkoutWithReps(_PushUpWorkout(), 25),
  WorkoutWithReps(_PushUpWorkout(), 50),
  WorkoutWithReps(_ChairDipWorkout(), 20),
  WorkoutWithReps(_ChairDipWorkout(), 50),
  WorkoutWithReps(_ChairRowWorkout(), 30),
  WorkoutWithReps(_ChairRowWorkout(), 50),
  WorkoutWithReps(_SitUpWorkout(), 30),
  WorkoutWithReps(_SitUpWorkout(), 100),
  WorkoutWithReps(_BentLegRaiseWorkout(), 25),
  WorkoutWithReps(_BentLegRaiseWorkout(), 50),
  WorkoutWithReps(_BentOverTwistWorkout(), 25),
  WorkoutWithReps(_BentOverTwistWorkout(), 50),
  WorkoutWithReps(_SquatsWorkout(), 25),
  WorkoutWithReps(_SquatsWorkout(), 70),
  WorkoutWithReps(_CalfRaiseWorkout(), 25),
  WorkoutWithReps(_CalfRaiseWorkout(), 50),
  WorkoutWithReps(_ChinUpWorkout(), 10),
  WorkoutWithReps(_ChinUpWorkout(), 30),
};

Workout from(String name) => workouts.firstWhere((workout) => workout.name == name);

mixin CompletableWorkout on Workout {
  bool isComplete();
}

abstract class Workout {
  final String name;
  final Image visual;

  Workout(this.name, this.visual);

  @override
  bool operator ==(dynamic other) {
    return other is Workout && this.name == other.name && this.visual == other.visual;
  }

  @override
  int get hashCode => hashValues(name, visual);
}

class WorkoutWithReps extends Workout implements CompletableWorkout {
  final int _maxReps;
  int _completedReps;

  WorkoutWithReps(Workout workout, this._maxReps, [this._completedReps = 0])
      : super(workout.name, workout.visual);

  @override
  bool isComplete() => _completedReps >= _maxReps;

  int getReps() => _maxReps;

  int getCompletedReps() => _completedReps;

  void addRep() => _completedReps++;
}

class _BentLegRaiseWorkout extends Workout {
  _BentLegRaiseWorkout() : super("Bent Leg Raise", Image.asset(Strings.workouts.bentLegRaise));
}

class _BentOverTwistWorkout extends Workout {
  _BentOverTwistWorkout() : super("Bent Over Twist", Image.asset(Strings.workouts.bentOverTwist));
}

class _CalfRaiseWorkout extends Workout {
  _CalfRaiseWorkout() : super("Calf raise", Image.asset(Strings.workouts.calfRaise));
}

class _ChinUpWorkout extends Workout {
  _ChinUpWorkout() : super("Chin-ups", Image.asset(Strings.workouts.chinUp));
}

class _ChairDipWorkout extends Workout {
  _ChairDipWorkout() : super("Dip between chairs", Image.asset(Strings.workouts.chairDip));
}

class _PushUpWorkout extends Workout {
  _PushUpWorkout() : super("Push-ups", Image.asset(Strings.workouts.pushUp));
}

class _ChairRowWorkout extends Workout {
  _ChairRowWorkout() : super("Row between chairs", Image.asset(Strings.workouts.chairRow));
}

class _SitUpWorkout extends Workout {
  _SitUpWorkout() : super("Sit-ups", Image.asset(Strings.workouts.sitUp));
}

class _SquatsWorkout extends Workout {
  _SquatsWorkout() : super("Squats", Image.asset(Strings.workouts.squats));
}
