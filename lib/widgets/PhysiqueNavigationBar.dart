import 'package:flutter/material.dart'
    show
        BottomNavigationBar,
        BottomNavigationBarItem,
        BottomNavigationBarType,
        BuildContext,
        Colors,
        Icon,
        Icons,
        Key,
        State,
        StatefulWidget,
        Text,
        Widget;
import 'package:physique/constants/constants.dart' show Strings;
import 'package:rxdart/rxdart.dart' show BehaviorSubject, Subject;

class PhysiqueNavigationBar extends StatefulWidget {
  final Subject<int> _subject = BehaviorSubject<int>();

  PhysiqueNavigationBar({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PhysiqueNavigationBarState();

  void dispose() => _subject.close();

  void addListener(Function(int) listener) => _subject.listen(listener);

  void notify(int event) => _subject.add(event);
}

class _ScheduleNavItem extends BottomNavigationBarItem {
  _ScheduleNavItem()
      : super(icon: Icon(Icons.schedule), title: Text(Strings.schedule));
}

class _SettingsNavItem extends BottomNavigationBarItem {
  _SettingsNavItem()
      : super(icon: Icon(Icons.settings), title: Text(Strings.settings));
}

class _PhysiqueNavigationBarState extends State<PhysiqueNavigationBar> {
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    widget.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          _ScheduleNavItem(),
          _SettingsNavItem()
        ],
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.amber,
        currentIndex: _selectedIndex,
        onTap: (int index) {
          setState(() {
            _selectedIndex = index;
            widget.notify(index);
          });
        });
  }
}
