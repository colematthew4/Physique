import 'package:flutter/material.dart'
    show
        AppBar,
        AppLifecycleState,
        BuildContext,
        Scaffold,
        State,
        StatefulWidget,
        Text,
        Widget,
        WidgetsBinding,
        WidgetsBindingObserver;
import 'package:physique/constants/constants.dart';
import 'PhysiqueNavigationBar.dart';
import 'package:physique/pages/pages.dart' show DashboardPageBuilder;

class PhysiqueDashboard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() =>
      _PhysiqueDashboardState(PhysiqueDashboardTab.schedule);
}

class _PhysiqueDashboardState extends State<PhysiqueDashboard>
    with WidgetsBindingObserver {
  int _selectedTabIndex;

  _PhysiqueDashboardState(PhysiqueDashboardTab startingTab)
      : _selectedTabIndex = startingTab.index;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
//    if (state == AppLifecycleState.resumed) {
//
//    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(Strings.applicationName)),
        body: DashboardPageBuilder(_selectedTabIndex),
        bottomNavigationBar: PhysiqueNavigationBar()
          ..addListener((int index) {
            setState(() {
              _selectedTabIndex = index;
            });
          }));
  }
}
